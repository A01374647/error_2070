package mx.itesm.error_2070;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;

/**
 * SplashScreen. Muestra el logo del Tec por 2 segundos
 */

class PantallaInicial extends Pantalla{

    private float tiempoVisible = 3.6f;

    // Es la referencia a la aplicación (la única que puede cambiar pantallas)
    private Error juego;

    // Logo del tec
    private Texture texturaLogo;
    private Sprite spriteLogo;

    // Constructor, guarda la referencia al juego
    public PantallaInicial(Error error){this.juego = error;}

    @Override
    public void show(){
        texturaLogo = new Texture(Gdx.files.internal("Fondos/azulTec.jpg"));
        spriteLogo = new Sprite(texturaLogo);
        spriteLogo.setPosition(ANCHO/2-spriteLogo.getWidth()/2, ALTO/2-spriteLogo.getHeight()/2);
        escalarLogo();
    }

    // hace que mantenga la proporción ancho-alto. (SOLO en landscape)
    private void escalarLogo(){
        float factorCamara = ANCHO / ALTO;
        float factorPantalla = 1.0f*Gdx.graphics.getWidth() / Gdx.graphics.getHeight();
        float escala = factorCamara / factorPantalla;
        spriteLogo.setScale(escala, 1);
    }

    @Override
    public void render(float delta){

        // Dibujar
        borrarPantalla(1,1,1);

        batch.setProjectionMatrix(camara.combined);
        batch.begin();
        // Dibuja el logo centrado
        spriteLogo.draw(batch);
        batch.end();

        // Actualizar para cambiar pantalla
        tiempoVisible -= delta;
        if (tiempoVisible<=0){ // Se acabaron los dos segundos
            juego.setScreen(new PantallaCargando(juego, Pantallas.MENU));
        }
    }

    @Override
    public void actualizarVista(){escalarLogo();}

    @Override
    public void pause(){}

    @Override
    public void resume(){}

    @Override
    public void dispose(){texturaLogo.dispose();}

}
