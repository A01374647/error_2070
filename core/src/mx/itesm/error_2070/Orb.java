package mx.itesm.error_2070;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * Created by Daniel Alillo on 31/10/2017.
 */

public class Orb extends Objeto{

    private final float VELOCIDAD_X = -375;
    private Texture texture;

    public Orb(Texture textura, float x, float y){
        super(textura, x, y);
        this.texture = textura;
    }

    public void dibujar(SpriteBatch batch){
        batch.draw(texture, sprite.getX(), sprite.getY());
    }

    public void mover(float delta){
        float distancia = VELOCIDAD_X*delta;
        sprite.setX(sprite.getX()+distancia);
    }

    public float getX(){return sprite.getX();}

    public boolean chocaCon(RobotRunner robotRunner){
        return sprite.getBoundingRectangle().overlaps(robotRunner.sprite.getBoundingRectangle());
    }

}
