package mx.itesm.error_2070;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * Created by Daniel Alillo on 29/10/2017.
 */

public class Bomba extends Objeto{

    private final float VELOCIDAD_X = -370;
    private Animation<TextureRegion> spriteAnimado;
    private float timerAnimacion;

    public Bomba(Texture textura, float x, float y){

        TextureRegion texturaCompleta = new TextureRegion(textura);

        TextureRegion[][] texturaBomba = texturaCompleta.split(134,127);

        spriteAnimado = new Animation(0.08f, texturaBomba[0][0], texturaBomba[0][1], texturaBomba[0][2],
                texturaBomba[0][3]);
        // Animación infinita
        spriteAnimado.setPlayMode(Animation.PlayMode.LOOP);
        // Inicia el timer que contará tiempo para saber qué frame se dibuja
        timerAnimacion = 0;
        // Crea el sprite con el personaje quieto (idle)
        sprite = new Sprite(texturaBomba[0][3]);    // PAUSA
        sprite.setPosition(x,y);
    }

    // Dibuja el hongo
    public void dibujar(SpriteBatch batch, EstadoJuego juego){
        switch (juego){
            case JUGANDO:
                timerAnimacion += Gdx.graphics.getDeltaTime();
                TextureRegion region = spriteAnimado.getKeyFrame(timerAnimacion);
                batch.draw(region,sprite.getX(),sprite.getY());
                break;
            case PAUSADO:
                sprite.draw(batch); // Dibuja el sprite estático
                break;
        }
    }

    // Mueve el personaje a la izquierda
    public void mover(float delta){
        float distancia = VELOCIDAD_X*delta;
        sprite.setX(sprite.getX()+distancia);
    }

    public float getX(){return sprite.getX();}

    public boolean chocaCon(RobotRunner robotRunner){
        return sprite.getBoundingRectangle().overlaps(robotRunner.sprite.getBoundingRectangle());
    }

}
